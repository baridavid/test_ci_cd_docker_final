﻿using System.Web;
using System.Web.Mvc;

namespace Test_CI_CD_Docker_Final
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
